
package grafica;

import org.jfree.data.xy.*;
import org.jfree.chart.*;
import org.jfree.chart.plot.*;
import java.awt.image.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import java.io.File;
import org.jfree.chart.plot.*;
import java.io.*;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.data.category.DefaultCategoryDataset;


/**
 *
 * @author Nahum
 */
public class Grafica extends java.awt.Frame{
    
      int o;
      BufferedImage grafica = null;

    //constructor
    public Grafica(String title) {
        super(title);
       
    }
    public Grafica(){
        
    }
     public void extra(int l){
         
       Leer1 lee = new Leer1();
        Map misDatos = null;
        try{
            misDatos = lee.extraDatos("fdp.txt");
        }catch(IOException ex){
            Logger.getLogger(Leer1.class.getName()).log(Level.SEVERE,null,ex);
        }
        
        double n=0;
        double xdn=0;
        double m;
        m = misDatos.size();
        System.out.println("Total de datos: "+m);
        System.out.println("Dominio --> Codominio\n");
        for (int i = 0; i < m; i++) {
            n = i;
            xdn = (Double)misDatos.get(new Double(i));
            System.out.println(n+" ----> "+xdn+"\n");
        }
        
        creaImagen(l);
    }

    public BufferedImage creaImagen(int l)
    {
        
        Leer1 lee = new Leer1();
        Map misDatos = null;
        try{
            misDatos = lee.extraDatos("fdp.txt");
        }catch(IOException ex){
            Logger.getLogger(Leer1.class.getName()).log(Level.SEVERE,null,ex);
        }
       
        XYSeries series = new XYSeries("titulo de la serie");
   
        double xdn=0;
        double m;
        m = misDatos.size();
        if(l == 1){
            for (int i = 0; i < m; i++) {
            System.out.println(i+" "+ (xdn = (Double)misDatos.get(new Double(i))));
            series.add(i, xdn);
            }
        }else if(l == 2){
            for (int i = 0; i < m; i++) {
            System.out.println(i+" "+ (xdn += (Double)misDatos.get(new Double(i))));
            series.add(i, xdn);
            }
        }
        
        
        XYDataset juegoDatos= new XYSeriesCollection(series);
        
        
        if(l == 1){
            JFreeChart chart = ChartFactory.createXYLineChart        ("FDP",
            "X","F(X)",juegoDatos,PlotOrientation.VERTICAL,
            false,
            false,
            true             
            );
            BufferedImage image = chart.createBufferedImage(400,400);
            return image;
        }else if(l == 2){
            JFreeChart chart = ChartFactory.createXYLineChart        ("FDA",
            "X","F(X)",juegoDatos,PlotOrientation.VERTICAL,
            false,
            false,
            true             
            );
            BufferedImage image = chart.createBufferedImage(400,400);
            return image;
        }
     
          return null;
        
    }
    
    public int getO() {
        return o;
    }

    public void setO(int o) {
        this.o = o;
    }
   
    public void paint(java.awt.Graphics g) {

        if(grafica == null)
        {
            grafica = this.creaImagen(getO());
        }
        g.drawImage(grafica,30,30,null);
    }

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        Grafica miventana = new Grafica("Prueba");
        int n;
        
        System.out.println("Que quieres graficar");
        System.out.println("1:Funcion de Desnsidad de Probabilidad");
        System.out.println("2:Funcion de distribucion Acumulada");
        n=sc.nextInt();
        
        switch(n){
            case 1:
                miventana.extra(n);
                miventana.setO(n);
                miventana.pack();
                miventana.setSize(450,450);
                miventana.setVisible(true);
            break;
            case 2:
                miventana.extra(n);
                miventana.setO(n);
                miventana.pack();
                miventana.setSize(450, 450);
                miventana.setVisible(true);
                break;
        }
    }
    
}
