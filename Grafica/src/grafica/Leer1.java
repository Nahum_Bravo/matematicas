/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafica;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 *
 * @author Nahum
 */
public class Leer1 {
    
    public Map extraDatos(String nombreArchivo) throws FileNotFoundException, IOException{
     Map datos = null;
        FileInputStream fis = new FileInputStream(nombreArchivo);
        DataInputStream dis = new DataInputStream(fis);
        BufferedReader br = new BufferedReader(new InputStreamReader(dis));
    
     String temp=""; 
     datos = new HashMap();
     StringTokenizer st = null;
     while((temp = br.readLine()) != null){
         st = new StringTokenizer(temp,",");
         while(st.hasMoreTokens()){
             datos.put((Double.parseDouble(st.nextToken())), (Double.parseDouble(st.nextToken())));
         }
     }
     return datos;
    } 
}
